import { BrowserModule } from '@angular/platform-browser';
import { NgModule,CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { AngularMaterialModule } from './angular-material.module';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NavbarComponent } from './navbar/navbar.component';
import {HomepageComponent} from "./homepage/homepage.component";
import { FooterComponent } from './footer/footer.component';
import { AmministratoreComponent } from './amministratore/amministratore.component';
import { MagazzinoComponent } from './amministratore/pagina-interna-amm/magazzino/magazzino.component';
import { ModaleMagazzinoComponent } from './amministratore/pagina-interna-amm/magazzino/modale-magazzino/modale-magazzino.component';
import { CardHeaderComponent } from './amministratore/pagina-interna-amm/magazzino/card-header/card-header.component';
import { ModaleAddProdottoComponent } from './amministratore/pagina-interna-amm/magazzino/modale-add-prodotto/modale-add-prodotto.component';
import { OrdiniComponent } from "./amministratore/pagina-interna-amm/ordini/ordini.component";
import { ModaleDettaglioOrdineComponent } from './amministratore/pagina-interna-amm/ordini/modale-dettaglio-ordine/modale-dettaglio-ordine.component';
import { PaginaInternaAmmComponent } from './amministratore/pagina-interna-amm/pagina-interna-amm.component';
import { ProvaindirizzoComponent } from './provaindirizzo/provaindirizzo.component';
import { GeoapifyGeocoderAutocompleteModule } from '@geoapify/angular-geocoder-autocomplete';
import { HttpClientModule} from "@angular/common/http";
import { StoricoComponent } from './amministratore/pagina-interna-amm/storico/storico.component';
import { ModaleStoricoComponent } from './amministratore/pagina-interna-amm/storico/modale-storico/modale-storico.component';
import { ModaleEliminaComponent } from './amministratore/pagina-interna-amm/magazzino/modale-elimina/modale-elimina.component';
import { RegistrazioneComponent } from './amministratore/registrazione/registrazione.component'
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomepageComponent,
    FooterComponent,
    AmministratoreComponent,
    MagazzinoComponent,
    ModaleMagazzinoComponent,
    CardHeaderComponent,
    ModaleAddProdottoComponent,
    OrdiniComponent,
    ModaleDettaglioOrdineComponent,
    PaginaInternaAmmComponent,
    ProvaindirizzoComponent,
    StoricoComponent,
    ModaleStoricoComponent,
    ModaleEliminaComponent,
    RegistrazioneComponent,
   
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FlexLayoutModule ,
    AngularMaterialModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    
    
  ],
  providers: [],
  bootstrap: [AppComponent],
  schemas:[CUSTOM_ELEMENTS_SCHEMA ]
})
export class AppModule { }
