import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-registrazione',
  templateUrl: './registrazione.component.html',
  styleUrls: ['./registrazione.component.css']
})
export class RegistrazioneComponent implements OnInit {
  formRegAmm : FormGroup;
  constructor( private fb : FormBuilder) { }
  



  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);

  nomeNegozio = new FormControl('',[
    Validators.required,
  
  ])

  telefono = new FormControl('',[
    Validators.required,
    Validators.pattern("^[0-9]{10}$")
  ])


  ngOnInit(): void {
    this.formRegAmm = new FormGroup({});

    this.fb.group({
      emailFormControl : this.emailFormControl,
      nomeNegozio : this.nomeNegozio,
      telefono : this.telefono
    })
  }


}
