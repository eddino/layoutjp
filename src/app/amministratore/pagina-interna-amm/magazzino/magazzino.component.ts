import {AfterViewInit, Component, ViewChild,OnInit} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {animate, state, style, transition, trigger} from '@angular/animations';
import { MatPaginatorIntl } from '@angular/material/paginator';
import {MatDialog} from '@angular/material/dialog';
import { ModaleMagazzinoComponent } from './modale-magazzino/modale-magazzino.component';
import { ModaleAddProdottoComponent } from './modale-add-prodotto/modale-add-prodotto.component';
import { ModaleEliminaComponent } from './modale-elimina/modale-elimina.component';
import { TooltipPosition } from '@angular/material/tooltip';
import { FormControl } from '@angular/forms';



@Component({
  selector: 'app-magazzino',
  templateUrl: './magazzino.component.html',
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
  styleUrls: ['./magazzino.component.css'],

})

export class MagazzinoComponent implements OnInit {
  displayedColumns: string[] = ['id', 'nome', 'quantita', 'prezzo','actions'];
  dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);
  positionOptions: TooltipPosition[] = ['below', 'above', 'left', 'right'];
  position = new FormControl(this.positionOptions[3]);
  hide = true;

  constructor(public dialog: MatDialog) { }
  @ViewChild(MatPaginator, {static:true} )paginator: MatPaginator;
  ngOnInit(): void {
    this.paginator._intl.itemsPerPageLabel="Elementi per pagina";


  }


  modaleModifica(){
    const dialogRef = this.dialog.open(ModaleMagazzinoComponent);

  }

  modaleElimina(){
    const dialogRef = this.dialog.open(ModaleEliminaComponent);
  }

  addProdotto(){
    const dialogRef = this.dialog.open(ModaleAddProdottoComponent);
  }

  
  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

}
export interface PeriodicElement {
  nome: string;
  id: number;
  quantita: number;
  prezzo: string;
  visualizza:boolean;
}

var ELEMENT_DATA: PeriodicElement[] = [
  {id: 1, nome: 'Hydrogen', quantita: 1.0079, prezzo: 'H',visualizza:true},
  {id: 2, nome: 'Helium', quantita: 4.0026, prezzo: 'He',visualizza:true},
  {id: 3, nome: 'Lithium', quantita: 6.941, prezzo: 'Li',visualizza:true},
  {id: 4, nome: 'Beryllium', quantita: 9.0122, prezzo: 'Be',visualizza:true},
  {id: 5, nome: 'Boron', quantita: 10.811, prezzo: 'B',visualizza:true},
  {id: 6, nome: 'Carbon', quantita: 12.0107, prezzo: 'C',visualizza:true},
  {id: 7, nome: 'Nitrogen', quantita: 14.0067, prezzo: 'N',visualizza:true},
  {id: 8, nome: 'Oxygen', quantita: 15.9994, prezzo: 'O',visualizza:true},
  {id: 9, nome: 'Fluorine', quantita: 18.9984, prezzo: 'F',visualizza:true},

];