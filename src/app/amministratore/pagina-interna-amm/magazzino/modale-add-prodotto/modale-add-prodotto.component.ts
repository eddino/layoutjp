import { Component, OnInit } from '@angular/core';
import {MatDialog,MatDialogRef} from '@angular/material/dialog';
@Component({
  selector: 'app-modale-add-prodotto',
  templateUrl: './modale-add-prodotto.component.html',
  styleUrls: ['./modale-add-prodotto.component.css']
})
export class ModaleAddProdottoComponent implements OnInit {

  constructor(public dialog: MatDialog, public dialogRef: MatDialogRef<ModaleAddProdottoComponent>) { }

  unitaMisura: Unita[] = [
    {value: 'kg', viewValue: 'Kg'},
    {value: 'pezzi', viewValue: 'Numero Pezzi'},

  ];

  ngOnInit(): void {
  }
  onNoClick() : void{
    this.dialogRef.close()
  }

}


interface Unita {
  value: string;
  viewValue: string;
}

