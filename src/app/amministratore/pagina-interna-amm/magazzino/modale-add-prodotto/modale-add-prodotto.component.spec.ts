import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModaleAddProdottoComponent } from './modale-add-prodotto.component';

describe('ModaleAddProdottoComponent', () => {
  let component: ModaleAddProdottoComponent;
  let fixture: ComponentFixture<ModaleAddProdottoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModaleAddProdottoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModaleAddProdottoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
