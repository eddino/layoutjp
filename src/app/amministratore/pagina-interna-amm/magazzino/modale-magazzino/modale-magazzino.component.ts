import { Component, OnInit } from '@angular/core';
import {MatDialog,MatDialogRef} from '@angular/material/dialog';
@Component({
  selector: 'app-modale-magazzino',
  templateUrl: './modale-magazzino.component.html',
  styleUrls: ['./modale-magazzino.component.css']
})
export class ModaleMagazzinoComponent implements OnInit {

  constructor(public dialog: MatDialog, public dialogRef: MatDialogRef<ModaleMagazzinoComponent>) { }
  text : string;
  textBtn : boolean;



  
  ngOnInit(): void {
    
  }


  unitaMisura: Unita[] = [
    {value: 'kg', viewValue: 'Kg'},
    {value: 'pezzi', viewValue: 'Numero Pezzi'},

  ];

  onNoClick() : void{
    this.dialogRef.close()
  }

}

interface Unita {
  value: string;
  viewValue: string;
}
