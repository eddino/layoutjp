import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-modale-elimina',
  templateUrl: './modale-elimina.component.html',
  styleUrls: ['./modale-elimina.component.css']
})
export class ModaleEliminaComponent implements OnInit {

  constructor(public dialog: MatDialog, public dialogRef: MatDialogRef<ModaleEliminaComponent>) { }

  ngOnInit(): void {
  }

  onNoClick() : void{
    this.dialogRef.close()
  }

}
