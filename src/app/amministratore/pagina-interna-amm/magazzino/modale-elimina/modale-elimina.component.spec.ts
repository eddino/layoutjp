import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModaleEliminaComponent } from './modale-elimina.component';

describe('ModaleEliminaComponent', () => {
  let component: ModaleEliminaComponent;
  let fixture: ComponentFixture<ModaleEliminaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModaleEliminaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModaleEliminaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
