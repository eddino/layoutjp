import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PaginaInternaAmmComponent } from './pagina-interna-amm.component';

describe('PaginaInternaAmmComponent', () => {
  let component: PaginaInternaAmmComponent;
  let fixture: ComponentFixture<PaginaInternaAmmComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PaginaInternaAmmComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PaginaInternaAmmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
