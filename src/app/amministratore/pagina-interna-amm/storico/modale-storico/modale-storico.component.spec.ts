import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModaleStoricoComponent } from './modale-storico.component';

describe('ModaleStoricoComponent', () => {
  let component: ModaleStoricoComponent;
  let fixture: ComponentFixture<ModaleStoricoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModaleStoricoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModaleStoricoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
