import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';
@Component({
  selector: 'app-amministratore',
  templateUrl: './amministratore.component.html',
  styleUrls: ['./amministratore.component.css']
})
export class AmministratoreComponent implements OnInit {

  constructor(private router: Router,) { }

  ngOnInit(): void {
  }

  gestMag(){
    this.router.navigate(["gestione-attivita"]);
  }

}
