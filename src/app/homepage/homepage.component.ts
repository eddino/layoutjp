import { Component, OnInit, ViewChild } from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations';
import Typewriter from 't-writer.js';
import * as AOS from 'aos';
import { Router } from '@angular/router';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css'],
  
})
export class HomepageComponent implements OnInit {

  @ViewChild('tw3') typewriterElement3;

  constructor(private router: Router,) { }

  ngOnInit(): void {
    AOS.init();
  }

  regAmm(){
    this.router.navigate(["partners"]);
  }
  ngAfterViewInit() {
    this.typeWriter();
  }


  typeWriter(){
    const target3 = this.typewriterElement3.nativeElement;
    const writer3 = new Typewriter(target3, {
      typeColor: '#fff',
      typeSpeed: 40,
      deleteSpeed: 10,
      loop:true
    })



writer3
  .type("Ordina i tuoi prodotti preferiti e controllane la disponibilità come e quando vuoi")
  .rest(100)
  .clear()
  .changeTypeColor('#fff')
  .type("Prenota e rivivi la sensazione di acquistare dal tuo fornitore preferito ")
  .rest(100)
  .clear()
  .changeTypeColor('#fff')
  .start()
  }



  

  prova(){
    console.log("ese")
  }

}
